# ueberauth_gab Code of Conduct

Politics play an (unfortunately) increasing role in software development. In large part this can be blamed on being too welcoming and accommodating to those whose interest in the quality of the projects they contribute to is equalled or often far exceeded by their interest in pushing often divisive and disruptive ideological views.

By its very nature, this project already has some unavoidable political influence at its core. Gab.ai exists as a reaction to the shamelessly biased administration of and censorship by companies with a near or outright monopoly in their respective social media segments. By supporting Gab.ai, we also implicitly support for a free and open internet which fosters freedom of speech.

The owners of this project believe you have a right to speak your mind. That does not mean you necessarily have a right to be heard by any particular audience. In that spirit we will do our best to maintain an open atmosphere which welcomes differing viewpoints and opinions. If those viewpoints and opinions are fundamentally contrary to our core values or ultimately more disruptive than constructive, they do not have a place here.

Our foremost goal here is writing awesome software. Stability, efficiency, user experience and maintainability are paramount. If you wish to contribute towards these ends, your contribution is equally welcome regardless of your background. Characteristics such as your age, sex and ethnicity are all equally as relevant to this project as your favourite movie, shoe size, star sign, i.e. they aren't relevant. Where personal characteristics are subject to personal choice such as with religion or political alignment, while you have a right to express your views this is probably not the best place to do so if they will cause conflict.

There is zero tolerance for targetted harassment. At the same time there is zero tolerance for virtue signalling and willful victimhood. Be nice to people, and when in doubt assume that they are being nice to you.

All that said, we want to foster an atmosphere that welcomes open and honest discussion within a climate of fun rather than fear. So long as it does not compromise the quality of this project in any way or cause any personal or professional harm to others, personal expression is welcomed. At the end of the day we are all people after all.

The project owners possess governing power over the project in all respects they choose, be they technical, structural, organizational, personal, or otherwise. Should participants disagree with actions or decisions undertaken by the owners, they retain full right to voice their concerns, to fork the project if available, or to cease their activity within the project.

By contributing to the project, participants agree to follow this Code both in letter
and in the spirit of meritocracy embodied herein.
