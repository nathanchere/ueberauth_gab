# Überauth Gab

> Gab.ai strategy for Überauth.

## Code of Conduct

Please see [CODE_OF_CONDUCT](https://gitlab.com/nathanchere/ueberauth_gab/blob/master/CODE_OF_CONDUCT.md) for how to conduct yourself if wishing to contribute to this project. Or don't. In short: don't be a douche.

## License

Please see [LICENSE](https://gitlab.com/nathanchere/ueberauth_gab/blob/master/LICENSE) for licensing details.
